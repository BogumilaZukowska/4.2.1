# Moje README

*pierwszy paragraf*

**drugi paragraf**

~~czwarty~~ trzeci paragraf

>Cytat

Lista zagnieżdżona numeryczna

1. Jeden
2. Dwa
	1. Dwa jeden
	2. Dwa dwa

Lista zagnieżdżona nienumeryczna

+ jeden
+ dwa
+ trzy
	- trzy jeden
	- trzy dwa
		* trzy dwa jeden
		* trzy dwa dwa
	- trzy trzy

Kod:
```py
number1 = 10
number2 = 3
if number1 > number2:
	print(number1)
elif number1 < number2:
	print(number2)
else
	print('They are equal.')
```

Inny kod: `print("Hello in my repo!")`

Obrazek:
![lilia.jpeg](lilia.jpeg)
