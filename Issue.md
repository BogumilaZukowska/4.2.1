## Tabela: (#id1)

|Kolumna 1 |Kolumna 2 |Kolumna 3 |
|:---: |:---: |:---: |
|jeden |a |I |
|dwa |b |II |
|trzy |c |III |

## Kod w języku[^1] Python: (#id2)

```py
tab = [1,2,3,4,5,6,7,8]
suma = 0
for i in range(len(tab)):
	suma += tab[i]
print("Suma liczb w tabeli wynosi ",suma)
```
# Lista zadań (#id3)
- [x] Zadanie 1.
- [ ] Zadanie 2.
- [ ] Zadanie 3.
- [ ] Zadanie 4.

### Emotikony: (#id4)
:blue_heart:
:sparkles:
:foggy:
:panda_face:

[^1]: programowania

[Tabela](#id1)
[Kod w Pythonie](#id2)
[Lista zadań](#id3)
[Emotikony](#id4)
